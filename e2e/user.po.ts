import { browser, by, element } from 'protractor';

export class UserPage {

  navigateTo(id) {
    return browser.get('/user/' + id);
  }

  getFirstName() {
    return element.all(by.css('#name'));
  }

  getLastName() {
    return element.all(by.css('#lastName'));
  }

  getSaveButton() {
    return element.all(by.css('.btn.btn-success'));
  }
}
