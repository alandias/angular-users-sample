import { browser, by, element } from 'protractor';

export class UserListPage {
  navigateTo() {
    return browser.get('/');
  }

  getRows() {
    return element.all(by.css('.user-data'));
  }

  getFirstUserName() {
    return element.all(by.css('.name-user')).get(0).all(by.css('span')).getText();
  }

  getFirstUserId() {
    return element.all(by.css('.user-data')).get(0).all(by.css('.custom-control-input')).get(0).getAttribute('id');
  }

  getDetailsButton() {
    return element.all(by.css('.button-user')).get(0).all(by.css('button.btn-default'));
  }
}
