import { UserListPage } from './user-list.po';
import { UserPage } from './user.po';
import { browser, by } from 'protractor';

describe('go to the first user than edit the first and last name', () => {
  let page: UserListPage;
  let userPage: UserPage;

  beforeEach(() => {
    page = new UserListPage();
    userPage = new UserPage();
  });

  function presenceOfAll(elementArrayFinder) {
    return function () {
      return elementArrayFinder.count(function (count) {
        return count > 0;
      });
    };
  }

  it('should wait for the first user than click at the details button, go editing than check', () => {

    page.navigateTo();

    let rows = page.getRows();
    browser.wait(function () {
      return rows.count().then(function (countValue) {

        if (countValue > 0) {

          page.getFirstUserId().then(function (id) {

            expect(id.length).not.toEqual(0);

            page.getDetailsButton().click();

            expect(browser.getCurrentUrl()).toContain('/user/');

            userPage.navigateTo(id.split('_')[1]);

            userPage.getFirstName().clear().then(function () {

              userPage.getFirstName().sendKeys('Alan');

              userPage.getLastName().clear().then(function () {

                userPage.getLastName().sendKeys('Dias');

                userPage.getSaveButton().click();

                expect(browser.getCurrentUrl()).not.toContain('/user/');

                let updatedRows = page.getRows();
                browser.wait(function () {
                  return updatedRows.count().then(function (countUpdatedValue) {

                    if (countUpdatedValue > 0) {

                      page.getFirstUserName().then(function (textUpdated) {

                        expect(textUpdated[0]).toEqual('Alan Dias');

                      });

                      return true;
                    }

                  });

                });

              });

            });

          });

          return true;
        }

      });
    }, 5000);

  });
});
