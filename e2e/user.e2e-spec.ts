import { UserPage } from './user.po';
import { browser, by } from 'protractor';

describe('update the first and the last name then save', () => {

  let page: UserPage;

  beforeEach(() => {
    page = new UserPage();
  });

  function presenceOfAll(elementArrayFinder) {
    return function () {
      return elementArrayFinder.count(function (count) {
        return count > 0;
      });
    };
  }

  it('should update the first and last name then click to save', () => {

    page.navigateTo(1);

    page.getFirstName().sendKeys('Alan');

    page.getLastName().sendKeys('Dias');

    page.getSaveButton().click();

    expect(browser.getCurrentUrl()).not.toContain('/user/');

  });
});
