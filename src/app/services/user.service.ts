import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import 'rxjs/add/operator/map';
import { Observable } from 'rxjs';
import { environment } from '../../environments/environment';
import { User } from '../models/user';

@Injectable()
export class UserService {

  baseUrl: string = '';

  constructor(private http: Http) {

    if (!environment.jsonData) {
      //Supposing that '/users' is the route for Users in the Api
      this.baseUrl = environment.apiUrl + '/users';
    }
  }

  get() {

    if (environment.jsonData) {

      let usersFromStorage = localStorage.getItem('usersList');

      if (usersFromStorage && usersFromStorage !== null) {

        let usersList = JSON.parse(usersFromStorage);

        return Observable.create(observer => {
          setTimeout(() => {
            observer.next(usersList);
            observer.complete();
          }, 0);

        });

      } else {

        return this.http.get(environment.jsonData ? environment.usersJsonUrl : this.baseUrl)
          .map(response => {
            localStorage.setItem('usersList', JSON.stringify(response.json()));
            return response.json();
          });
      }

    } else {
      return this.http.get(this.baseUrl).map(response => response.json());
    }
  }

  getById(id) {

    if (environment.jsonData) {

      let usersFromStorage = localStorage.getItem('usersList');
      if (usersFromStorage && usersFromStorage !== null) {

        let list = JSON.parse(usersFromStorage);
        list = list.filter(user => user.id === id);
        if (!list || list.length === 0) {

          return Observable.throw({ msg: 'User not found!' });

        } else {

          return Observable.create(observer => {
            setTimeout(() => {
              observer.next(list[0]);
              observer.complete();
            }, 0);

          });
        }

      } else {

        return this.http.get(environment.usersJsonUrl)
          .map(response => {

            let list = response.json();
            localStorage.setItem('usersList', JSON.stringify(response.json()));
            list = list.filter(user => user.id === id);

            if (!list || list.length === 0) {
              throw new Error('User not found!');
            } else {
              return list[0];
            }

          });

      }

    } else {
      //Supposing that '/getById/:id' is the route for getting single User in the Api
      return this.http.get(this.baseUrl + '/getById/' + id).map(response => response.json());
    }
  }

  save(user: User) {

    if (environment.jsonData) {

      let usersFromStorage = localStorage.getItem('usersList');
      if (usersFromStorage && usersFromStorage !== null) {

        let list = JSON.parse(usersFromStorage);
        let userToBeUpdated = list.filter(u => u.id === user.id)[0];

        if (userToBeUpdated && userToBeUpdated.id > 0) {

          let index = list.indexOf(userToBeUpdated);
          list[index] = user;
          localStorage.setItem('usersList', JSON.stringify(list));

          return Observable.create(observer => {
            setTimeout(() => {
              observer.next({ msg: 'Changes saved successfully!' });
              observer.complete();
            }, 0);

          });

        } else {
          return Observable.throw({ msg: 'This User was not found to be updated!' });
        }

      } else {
        return Observable.throw({ msg: 'An error has occurred while trying to update the User!' });
      }

    } else {
      return this.http.post(this.baseUrl, user).map(response => response.json());
    }
  }

  delete(user) {

    if (environment.jsonData) {

      let usersFromStorage = localStorage.getItem('usersList');
      if (usersFromStorage && usersFromStorage !== null) {

        let list = JSON.parse(usersFromStorage);
        let userToBeDeleted = list.filter(u => u.id === user.id)[0];

        if (userToBeDeleted && userToBeDeleted.id > 0) {

          let index = list.indexOf(userToBeDeleted);
          list.splice(index, 1);
          localStorage.setItem('usersList', JSON.stringify(list));

          return Observable.create(observer => {
            setTimeout(() => {
              observer.next({ msg: 'User deleted successfully!' });
              observer.complete();
            }, 0);

          });

        } else {
          return Observable.throw({ msg: 'This User was not found to be deleted!' });
        }

      } else {
        return Observable.throw({ msg: 'An error has occurred while trying to delete the User!' });
      }

    } else {
      return this.http.delete(this.baseUrl, user).map(response => response.json());
    }
  }

  deleteList(users) {

    if (environment.jsonData) {

      let usersFromStorage = localStorage.getItem('usersList');
      if (usersFromStorage && usersFromStorage !== null) {

        let list = JSON.parse(usersFromStorage);

        for (let i = 0; i < users.length; i++) {

          let user = users[i];
          let userToBeDeleted = list.filter(u => u.id === user.id)[0];

          if (userToBeDeleted && userToBeDeleted.id > 0) {

            let index = list.indexOf(userToBeDeleted);
            list.splice(index, 1);

          } else {
            return Observable.throw({ msg: 'The user ' + userToBeDeleted.firstName + ' was not found to be deleted!' });
          }

        }

        localStorage.setItem('usersList', JSON.stringify(list));

        return Observable.create(observer => {
          setTimeout(() => {
            observer.next({ msg: 'User' + (users.length > 1 ? 's' : '') + ' deleted successfully!' });
            observer.complete();
          }, 0);

        });

      } else {
        return Observable.throw({ msg: 'An error has occurred while trying to delete the User!' });
      }

    } else {
      //Supposing that '/deleteList' is the route for deleting an array of Users in the Api
      return this.http.post(this.baseUrl + '/deleteList', users).map(response => response.json());
    }
  }
}