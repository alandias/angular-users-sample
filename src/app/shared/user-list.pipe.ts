import { Pipe, PipeTransform } from '@angular/core';

import { User } from '../models/user';

@Pipe({
    name: 'userFilter',
    pure: false
})
export class UserFilterPipe implements PipeTransform {

    transform(items: User[], filter: User): User[] {
        if (!items || !filter) {
            return items;
        }

        return items.filter((item: User) => this.applyFilter(item, filter));
    }

    applyFilter(user: User, filter: User): boolean {

        //Filter by any attribute inside the object

        // for (let field in filter) {
        //   if (filter[field]) {
        //     if (typeof filter[field] === 'string') {
        //       if (user[field].toLowerCase().indexOf(filter[field].toLowerCase()) === -1) {
        //         return false;
        //       }
        //     } else if (typeof filter[field] === 'number') {
        //       if (user[field] !== filter[field]) {
        //         return false;
        //       }
        //     }
        //   }
        // }

        //Filter by only first and last name
        if (user.firstName.toLowerCase().indexOf(filter.firstName.toLowerCase()) === -1 && 
                user.lastName.toLowerCase().indexOf(filter.firstName.toLowerCase()) === -1) {
            return false;
        }

        return true;
    }
}