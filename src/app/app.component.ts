import { Component } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent {
  
  alertOptions: AlertSettings;
  confirmationOptions: ConfirmationSettings;

  constructor() {
    this.alertOptions = {
      overlay: false,
      overlayClickToClose: false,
      showCloseButton: false,
      duration: 3600
    };

    this.confirmationOptions = {
      overlayClickToClose: true,
      showCloseButton: false,
    };
  }

}

interface AlertSettings {
  overlay?: boolean;
  overlayClickToClose?: boolean;
  showCloseButton?: boolean;
  duration?: number;
}

interface ConfirmationSettings {
  overlay?: boolean;
  overlayClickToClose?: boolean;
  showCloseButton?: boolean;
  confirmText?: string;
  declineText?: string;
}