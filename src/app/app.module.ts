import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { AngularFontAwesomeModule } from 'angular-font-awesome';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { RouterModule, Routes } from '@angular/router';
import { JasperoAlertsModule } from '@jaspero/ng2-alerts';
import { JasperoConfirmationsModule } from '@jaspero/ng2-confirmations';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';

import { AppComponent } from './app.component';
import { UserListComponent } from './components/user-list/user-list.component';
import { UserComponent } from './components/user/user.component';

import { UserService } from './services/user.service';
import { UserFilterPipe } from './shared/user-list.pipe';

const appRoutes: Routes = [
  { path: '', component: UserListComponent},
  { path: 'user/:id', component: UserComponent}
];

@NgModule({
  declarations: [
    AppComponent,
    UserListComponent,
    UserComponent,
    UserFilterPipe
  ],
  imports: [
    BrowserModule,
    AngularFontAwesomeModule,
    FormsModule,
    HttpModule,
    RouterModule.forRoot(appRoutes),
    JasperoAlertsModule,
    JasperoConfirmationsModule,
    BrowserAnimationsModule
  ],
  providers: [
    UserService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
