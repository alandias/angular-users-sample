import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { DomSanitizer } from '@angular/platform-browser';
import { User } from '../../models/user';
import { SafeUrl } from '@angular/platform-browser/src/security/dom_sanitization_service';
import { AlertsService, AlertType } from '@jaspero/ng2-alerts';
import { ConfirmationService } from '@jaspero/ng2-confirmations';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements OnInit {

  usersCollection: User[] = [];
  downloadJsonHref: SafeUrl;
  filter: User = new User();
  usersSelected: number = 0;
  isAllSelected: boolean = false;
  emptyList: boolean = true;

  constructor(private userService: UserService,
    private router: Router,
    private sanitizer: DomSanitizer,
    private _alert: AlertsService,
    private _confirmation: ConfirmationService) {


  }

  ngOnInit() {

    this.userService.get().subscribe((data) => {
      this.usersCollection = data;

      if(data !== null && data.length > 0){
        this.emptyList = false;
      }

    }, (error) => {

      let msg = error.msg ? error.msg : 'An error has occurred while trying to load the Users. Please try again later.';
      this._alert.create('warning', msg);

    });

  }

  formatUsersSelected() {
    if (this.usersSelected === 0) {
      return 'None user selected';
    } else {
      return this.usersSelected + (this.usersSelected === 1 ? ' user ' : ' users ') + 'selected';
    }
  }

  filterChanged() {
    this.clearSelection();
  }

  selectUser() {
    this.usersSelected = this.usersCollection.filter(user => user.isSelected).length;
    this.isAllSelected = this.usersSelected === this.usersCollection.length;
  }

  selectAll() {
    for (let i = 0; i < this.usersCollection.length; i++) {
      this.usersCollection[i].isSelected = true;
    }
    this.usersSelected = this.usersCollection.length;
    this.isAllSelected = true;
  }

  toogleAllChanged() {
    if(!this.isAllSelected){
      this.clearSelection();
    } else {
      this.selectAll();
    }
  }

  clearSelection() {
    for (let i = 0; i < this.usersCollection.length; i++) {
      this.usersCollection[i].isSelected = false;
    }
    this.usersSelected = 0;
    this.isAllSelected = false;
  }

  goToUser(user) {
    this.router.navigate(['/user/' + user.id]);
  }

  deleteUser(user) {

    this._confirmation.create('Want to delete this user?', user.firstName + (user.lastName && user.lastName.length > 0 ? (' ' + user.lastName) : ''))
      .subscribe((response) => {

        if (response.resolved === true) {

          this.userService.delete(user).subscribe((data) => {
            
            let index = this.usersCollection.indexOf(user);
            this.usersCollection.splice(index, 1);
            this.emptyList = this.usersCollection.length === 0;
            this.selectUser();
            this._alert.create('success', data.msg);
      
          }, (error) => {
      
            let msg = error.msg ? error.msg : 'An error occurred while attempting to delete the user. Please try again later.';
            this._alert.create('warning', msg);
      
          });
        }

      });
  }

  deleteSelection() {

    let usersSelected = this.usersCollection.filter(user => user.isSelected);
    if (usersSelected && usersSelected.length > 0) {

      this._confirmation.create('Want to delete this selection?', this.formatUsersSelected())
        .subscribe((response) => {

          if (response.resolved === true) {

            this.userService.deleteList(usersSelected).subscribe((data) => {
              
              for (let i = 0; i < usersSelected.length; i++) {
                let index = this.usersCollection.indexOf(usersSelected[i]);
                this.usersCollection.splice(index, 1);
              }

              this.emptyList = this.usersCollection.length === 0;
              this.clearSelection();
              this._alert.create('success', data.msg);
        
            }, (error) => {
        
              let msg = error.msg ? error.msg : 'An error occurred while attempting to delete the users. Please try again later.';
              this._alert.create('warning', msg);
        
            });
          }

        });

    } else {
      this._alert.create('warning', 'You must select at least one User!');
    }

  }

  download(blob, filename) {
    if (window.navigator.msSaveOrOpenBlob) {
      window.navigator.msSaveOrOpenBlob(blob, filename);
    } else {
      var a = document.createElement("a"),
        url = URL.createObjectURL(blob);
      a.href = url;
      a.download = filename;
      document.body.appendChild(a);
      a.click();
      setTimeout(function () {
        document.body.removeChild(a);
        window.URL.revokeObjectURL(url);
      }, 0);
    }
  }


  downloadSelection() {

    let usersSelected = this.usersCollection.filter(user => user.isSelected);
    if (usersSelected && usersSelected.length > 0) {

      let usersStr = JSON.stringify(usersSelected);
      let file = new Blob([usersStr], { type: 'text/json' });
      let date = new Date();
      let fileName = 'users.json';

      this.download(file, fileName);

    } else {
      this._alert.create('warning', 'You must select at least one User!');
    }

  }

}