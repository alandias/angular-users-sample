import { Component, OnInit } from '@angular/core';
import { UserService } from '../../services/user.service';
import { Router, ActivatedRoute, ParamMap } from '@angular/router';
import { User } from '../../models/user';
import { AlertsService, AlertType } from '@jaspero/ng2-alerts';
import { ConfirmationService } from '@jaspero/ng2-confirmations';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  constructor(private userService: UserService, 
              private route: ActivatedRoute, 
              private router: Router,
              private _alert: AlertsService,
              private _confirmation: ConfirmationService) { }

  usersCollection: User[] = [];
  user: User = new User();

  ngOnInit() {

    let id = Number(this.route.snapshot.paramMap.get('id'));
    this.userService.getById(id).subscribe((data) => {
      this.user = data;
    }, (error) => {
      let msg = error.msg ? error.msg : 'An error has occurred while trying to load the User. Please try again later.';
      this._alert.create('warning', msg);
      this.router.navigate(['/']);      
    });

  }

  onSubmit(){

    this.userService.save(this.user).subscribe((data) => {
      
      this._alert.create('success', data.msg);
      this.router.navigate(['/']);      

    }, (error) => {

      let msg = error.msg ? error.msg : 'An error occurred while attempting to update the user. Please try again later.';
      this._alert.create('warning', msg);
      this.router.navigate(['/']);

    });

  }

  goBack(form){

    if(!form.pristine){
      
      this._confirmation.create('Are you sure you want to discard the changes?', '')
      .subscribe((response) => {

        if (response.resolved === true) {
          this.router.navigate(['/']);
        }

      });

    } else {
      this.router.navigate(['/']);
    }
  }

}
