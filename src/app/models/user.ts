export class User {
    public id: number = 0;
    public firstName: string = '';
    public lastName: string = '';
    public age: number = 0;
    public description: string = '';
    public isSelected: boolean = false;

    constructor() {}
}