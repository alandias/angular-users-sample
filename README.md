# Users Sample in Angular 4

This project is a sample made in Angular 4, which works upon a list of users, giving the possibilities of filtering, editing and deleting.

The users are loaded by a json file and once they are loaded, they'll be saved to the LocalStorage of the browser.

Since then, the list will only get the values from the LocalStorage.

So, if you delete all the records on the main route,
you should delete the entry inside the LocalStorage, called `usersList`, so the application can reload it again.

## Installing the application

First things first, you need to install node in your computer, following these instructions: https://nodejs.org/en/download/

After that, install Angular Client following this link: https://cli.angular.io/

Navigate to a folder you want to keep the project, and clone the application through the command `git clone https://bitbucket.org/alandias/angular-users-sample.git` in your terminal.

Once it is done, run `npm install` (`sudo npm install` for Ubuntu) to install all dependencies for this project.

## Running the application

Still inside the terminal, go to the folder that was cloned with `cd angular-users-sample` and run `ng serve --open`.

The browser will open for you at this link:  `http://localhost:4200/`.

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor].

## Running unit tests

Run `ng test` to execute the unit tests via [Karma]. **Not implemented yet**